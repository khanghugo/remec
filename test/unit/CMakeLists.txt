FetchContent_Declare(catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2
  GIT_TAG v3.3.2
)
FetchContent_MakeAvailable(catch2)

function(add_remec_test TESTNAME)
  add_executable( ${TESTNAME}
                  ${TESTNAME}.cpp)

  target_link_libraries(  ${TESTNAME}
                          PRIVATE
                          Catch2::Catch2
                          ${PROJECT_NAME}_lib)

  add_test(NAME ${TESTNAME} COMMAND ${TESTNAME})
endfunction()

add_remec_test(TestEntity)
add_remec_test(TestParsing)
add_remec_test(TestUtils)
add_remec_test(TestVectorMath)
add_remec_test(TestVersion)
