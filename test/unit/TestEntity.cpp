#include "Utils.hpp"

#include <common/CustomEntity.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

struct MockEntity final : public CustomEntity<MockEntity> {
  using CustomEntity<MockEntity>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_mock";
  static constexpr inline auto Version             = "1.0.0";
  static constexpr inline auto Description         = "Mock";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override
  {
    return {};
  }

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept
  {
    static const std::vector<OwnProperty> props {
        OwnProperty {
            "ownprop1", 1337, "Custom property", OwnProperty::Type::String, {}},
        OwnProperty {"ownprop2",
                     "stringdefaultvalue",
                     "Custom property 2",
                     OwnProperty::Type::String,
                     {}},
        OwnProperty {"ownprop3",
                     10.03,
                     "Custom property 3",
                     OwnProperty::Type::String,
                     {}},
    };
    return props;
  }
};

const auto brushes = brushes_from_str(R"({
( -63 17 0 ) ( -63 59 -48 ) ( -63 59 0 ) TEXTURE_NAME [ 0 -1 0 -12 ] [ 0 0 -1 0 ] 0 1 1
( -8 40 0 ) ( -63 40 -48 ) ( -63 40 0 ) TEXTURE_NAME [ 1 0 0 1 ] [ 0 0 -1 0 ] 0 1 1
( -8 59 -48 ) ( -63 17 -48 ) ( -8 17 -48 ) TEXTURE_NAME [ -1 0 0 -1 ] [ 0 -1 0 -12 ] 0 1 1
( -8 59 0 ) ( -63 17 0 ) ( -63 59 0 ) TEXTURE_NAME [ 1 0 0 1 ] [ 0 -1 0 -12 ] 0 1 1
( -8 59 0 ) ( -63 59 -48 ) ( -8 59 -48 ) TEXTURE_NAME [ -1 0 0 -1 ] [ 0 0 -1 0 ] 0 1 1
( -8 59 0 ) ( -8 17 -48 ) ( -8 17 0 ) TEXTURE_NAME [ 0 1 0 12 ] [ 0 0 -1 0 ] 0 1 1
})");

const MockEntity mock {Context {},
                       42,
                       69,
                       {
                           {"targetname", "mocktargetname"},
                           {"classname", "remec_mock"},
                           {"prop1", "prop1value"},
                           {"prop2", "prop2value"},
                       },
                       brushes};

const MockEntity mock2 {Context {},
                        42,
                        69,
                        {
                            {"targetname", "mocktargetname"},
                            {"classname", "remec_mock"},
                            {"prop1", "prop1value"},
                            {"prop2", "prop2value"},
                            {"ownprop1", "1338"},
                            {"ownprop2", "stringcustomvalue"},
                            {"ownprop3", "11.11"},
                        },
                        brushes};

TEST_CASE("CustomEntity methods")
{
  REQUIRE(mock.getEntityClassName() == std::string(MockEntity::ClassName));

  REQUIRE(mock.getEntityClassName() == std::string(MockEntity::ClassName));
  REQUIRE(mock.getEntityDescription() == std::string(MockEntity::Description));
  REQUIRE(mock.isTargetNameMandatory() == MockEntity::TargetNameMandatory);
  REQUIRE(mock.isSolid() == MockEntity::Solid);

  const auto ownProps = mock.getOwnProperties();

  REQUIRE(ownProps->size() == 4);
  REQUIRE((*ownProps)[0].name == "ownprop1");
  REQUIRE((*ownProps)[1].name == "ownprop2");
  REQUIRE((*ownProps)[2].name == "ownprop3");
  REQUIRE((*ownProps)[3].name == "_tb_layer");
}

TEST_CASE("Entity.getProperties")
{
  const auto &props = mock.getProperties();

  REQUIRE(props.at("prop1") == "prop1value");
  REQUIRE(props.at("prop2") == "prop2value");
  REQUIRE(props.at("targetname") == "mocktargetname");
  REQUIRE(props.at("classname") == "remec_mock");
}

TEST_CASE("Entity.getFilteredProperties")
{
  SECTION("filter should remove targetname and classname")
  {
    const auto filtered = mock.getFilteredProperties();

    REQUIRE(filtered.at("prop1") == "prop1value");
    REQUIRE(filtered.at("prop2") == "prop2value");

    REQUIRE(filtered.count("targetname") == 0);
    REQUIRE(filtered.count("classname") == 0);
  }
}

TEST_CASE("Entity getters")
{
  REQUIRE(mock.getOriginalStartPos() == 42);
  REQUIRE(mock.getOriginalLen() == 69);
  REQUIRE(mock.getName() == "mocktargetname");

  REQUIRE(brushes_to_str(mock.getBrushes()) == brushes_to_str(brushes));
}

TEST_CASE("Entity.getOrDefault")
{
  SECTION("int")
  {
    SECTION("should return default value when property is not set")
    {
      REQUIRE(mock.getOrDefault<int>("ownprop1") == 1337);
    }

    SECTION("should return user value when property is set")
    {
      REQUIRE(mock2.getOrDefault<int>("ownprop1") == 1338);
    }
  }

  SECTION("string")
  {
    SECTION("should return default value when property is not set")
    {
      REQUIRE(mock.getOrDefault("ownprop2") == "stringdefaultvalue");
    }

    SECTION("should return user value when property is set")
    {
      REQUIRE(mock2.getOrDefault("ownprop2") == "stringcustomvalue");
    }
  }

  SECTION("double")
  {
    SECTION("should return default value when property is not set")
    {
      REQUIRE(mock.getOrDefault<double>("ownprop3") == 10.03);
    }

    SECTION("should return user value when property is set")
    {
      REQUIRE(mock2.getOrDefault<double>("ownprop3") == 11.11);
    }
  }
}

TEST_CASE("Entity.getOwnProperty")
{
  SECTION("regular properties should NOT be found")
  {
    REQUIRE_THROWS(mock.getOwnProperty("targetname"));
    REQUIRE_THROWS(mock.getOwnProperty("classname"));
    REQUIRE_THROWS(mock.getOwnProperty("prop1"));
  }

  SECTION("invalid properties should NOT be found")
  {
    REQUIRE_THROWS(mock.getOwnProperty("unknownprop"));
  }

  REQUIRE(mock.getOwnProperty("ownprop2").name == "ownprop2");
}
