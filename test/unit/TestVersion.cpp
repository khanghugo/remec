#include "util/Version.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("version_from_str")
{
  REQUIRE_THROWS(version_from_str(""));
  REQUIRE_THROWS(version_from_str("1"));
  REQUIRE_THROWS(version_from_str("1."));
  REQUIRE_THROWS(version_from_str("1.2"));
  REQUIRE_THROWS(version_from_str("1.2."));

  REQUIRE(version_from_str("1.0.0") == Version {1, 0, 0});
  REQUIRE(version_from_str("1.2.3") == Version {1, 2, 3});
  REQUIRE(version_from_str("2.1.0") == Version {2, 1, 0});

  REQUIRE(Version {1, 0, 0} < Version {1, 0, 1});
  REQUIRE(Version {1, 0, 0} < Version {1, 1, 0});
  REQUIRE(Version {1, 0, 0} < Version {1, 1, 1});
  REQUIRE(Version {1, 0, 0} < Version {2, 0, 0});
  REQUIRE(Version {1, 0, 0} < Version {2, 1, 0});
  REQUIRE(Version {1, 0, 0} < Version {2, 0, 1});
  REQUIRE(Version {1, 0, 0} < Version {2, 1, 1});

  REQUIRE(Version {1, 0, 1} > Version {1, 0, 0});
  REQUIRE(Version {1, 1, 0} > Version {1, 0, 0});
  REQUIRE(Version {1, 1, 1} > Version {1, 0, 0});
  REQUIRE(Version {2, 0, 0} > Version {1, 0, 0});
  REQUIRE(Version {2, 1, 0} > Version {1, 0, 0});
  REQUIRE(Version {2, 0, 1} > Version {1, 0, 0});
  REQUIRE(Version {2, 1, 1} > Version {1, 0, 0});
}
