#include "common/Face.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("get_normal_vector")
{
  {
    Face face {{64, 0, 64}, {0, 0, 64}, {64, 64, 64}};
    Point3D normal   = face.get_normal_vector();
    Point3D expected = {0, 0, -1};

    REQUIRE(normal == expected);
  }

  {
    Face face {{0, 0, 64}, {0, 64, 64}, {64, 64, 0}};
    Point3D normal   = face.get_normal_vector();
    Point3D expected = {-0.70711, 0, -0.70711};

    REQUIRE(normal == expected);
  }

  {
    Face face {
        {45, 0.254837, 0}, {-0.254837, -45, 64}, {-0.254837, 45.5097, 0}};
    Point3D normal   = face.get_normal_vector();
    Point3D expected = {-0.5, -0.5, -0.70711};

    REQUIRE(normal == expected);
  }

  {
    Face face {{0, -64, 384}, {0, -56, 0}, {-64, -64, 384}};
    Point3D normal   = face.get_normal_vector();
    Point3D expected = {0, 0.99978, 0.02083};

    REQUIRE(normal == expected);
  }

  {
    Face face {{100.856, 26.0894, 121.299},
               {-31.1357, -34.5276, 128},
               {152.267, 55.1017, 106.979}};
    Point3D normal   = face.get_normal_vector();
    Point3D expected = {0.36798, -0.84433, -0.38949};

    REQUIRE(normal == expected);
  }
}

TEST_CASE("get_face_offset")
{
  {
    Face face {{64, 0, 64}, {0, 0, 64}, {64, 64, 64}};
    double offset   = face.get_face_offset();
    double expected = -64;

    REQUIRE(std::abs(offset - expected) < Point3D::offset_epsilon);
  }

  {
    Face face {{0, 0, 64}, {0, 64, 64}, {64, 64, 0}};
    double offset   = face.get_face_offset();
    double expected = -45.25504;

    REQUIRE(std::abs(offset - expected) < Point3D::offset_epsilon);
  }

  {
    Face face {
        {45, 0.254837, 0}, {-0.254837, -45, 64}, {-0.254837, 45.5097, 0}};
    double offset   = face.get_face_offset();
    double expected = -22.6274185;

    REQUIRE(std::abs(offset - expected) < Point3D::offset_epsilon);
  }

  {
    Face face {{0, -64, 384}, {0, -56, 0}, {-64, -64, 384}};
    double offset   = face.get_face_offset();
    double expected = -55.9872;

    REQUIRE(std::abs(offset - expected) < Point3D::offset_epsilon);
  }

  {
    Face face {{100.856, 26.0894, 121.299},
               {-31.1357, -34.5276, 128},
               {152.267, 55.1017, 106.979}};
    double offset   = face.get_face_offset();
    double expected = -32.159819732;

    REQUIRE(std::abs(offset - expected) < Point3D::offset_epsilon);
  }
}

TEST_CASE("get_intersection_point")
{
  {
    Face face1 {{64, 64, 64}, {64, 64, 0}, {64, 0, 64}};
    Face face2 {{64, 0, 64}, {0, 0, 64}, {64, 64, 64}};
    Face face3 {{0, 64, 64}, {0, 64, 0}, {64, 64, 64}};

    std::optional<Point3D> intersection =
        get_intersection_point(face1, face2, face3);
    Point3D expected = {64, 64, 64};

    REQUIRE(intersection.value() == expected);
  }

  {
    Face face1 {{0, 0, 64}, {0, 0, 0}, {0, 64, 64}};
    Face face2 {{0, 64, 0}, {0, 0, 0}, {64, 64, 0}};
    Face face3 {{64, 0, 64}, {0, 0, 64}, {64, 64, 64}};

    std::optional<Point3D> intersection =
        get_intersection_point(face1, face2, face3);

    REQUIRE(intersection == std::nullopt);
  }

  {
    Face face1 {
        {-1024, -1024, -920}, {-1024, -1024, -1024}, {-1024, -975, -920}};
    Face face2 {{-1024, -975, -920}, {-1024, -896, -960}, {-976, -1024, -920}};
    Face face3 {
        {-1024, -1024, -1024}, {-1024, -1024, -920}, {-896, -1024, -1024}};

    std::optional<Point3D> intersection =
        get_intersection_point(face1, face2, face3);
    Point3D expected = {-1024, -1024, -895.1898734177215};

    REQUIRE(intersection.value() == expected);
  }

  {
    Face face1 {{-1717.25, -1297.75, -602.754},
                {-1896.54, -1168.8, -512},
                {-1664, -1267.43, -661.543}};
    Face face2 {{-1968.06, -1408, -600.193},
                {-1951.52, -1195.89, -512.694},
                {-1717.25, -1297.75, -602.754}};
    Face face3 {{-1896.54, -1168.8, -512},
                {-1951.52, -1195.89, -512.694},
                {-1854.16, -1057.9, -599.034}};

    std::optional<Point3D> intersection =
        get_intersection_point(face1, face2, face3);
    Point3D expected = {
        -1896.5383953001435, -1168.7958001081656, -512.0032961969650};

    REQUIRE(intersection.value() == expected);
  }

  {
    Face face1 {
        {-3840, -3584, -3840}, {-3616, -3584, -3840}, {-3840, -3584, -3584}};
    Face face2 {
        {-3584, -3584, -3584}, {-3616, -3584, -3840}, {-3584, -3616, -3584}};
    Face face3 {
        {-3840, -3840, -3616}, {-3840, -3616, -3584}, {-3588, -3840, -3616}};

    std::optional<Point3D> intersection =
        get_intersection_point(face1, face2, face3);
    Point3D expected = {
        -3583.4285714285715, -3584.0000000000000, -3579.4285714285718};

    REQUIRE(intersection.value() == expected);
  }
}

TEST_CASE("get_point_position_on_face")
{
  {
    Face face {{256, -256, 0}, {-256, -256, 0}, {256, 256, 0}};

    Point3D on1 {0, 0, 0};
    Point3D on2 {1000, 1000, 0};
    Point3D on3 {-1000, -1000, 0};
    Point3D on4 {0, 0, 0.0099};
    Point3D on5 {0, 0, -0.0099};

    REQUIRE(get_point_position_on_face(on1, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on2, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on3, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on4, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on5, face) == PointPosition::OnFace);

    Point3D below1 {0, 0, 10};
    Point3D below2 {0, 0, 0.01};
    Point3D below3 {100, 0, 1};
    Point3D below4 {100, 100, 1};
    Point3D below5 {0, 100, 100};

    REQUIRE(get_point_position_on_face(below1, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below2, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below3, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below4, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below5, face) ==
            PointPosition::BelowFace);

    Point3D above1 {0, 0, -10};
    Point3D above2 {0, 0, -0.01};
    Point3D above3 {100, 0, -1};
    Point3D above4 {100, 100, -1};
    Point3D above5 {0, 100, -100};

    REQUIRE(get_point_position_on_face(above1, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above2, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above3, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above4, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above5, face) ==
            PointPosition::AboveFace);
  }

  {
    Face face {{-256, 256, 0}, {-256, -256, 0}, {256, 256, 0}};

    Point3D on1 {0, 0, 0};
    Point3D on2 {1000, 1000, 0};
    Point3D on3 {-1000, -1000, 0};
    Point3D on4 {0, 0, 0.0099};
    Point3D on5 {0, 0, -0.0099};

    REQUIRE(get_point_position_on_face(on1, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on2, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on3, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on4, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on5, face) == PointPosition::OnFace);

    Point3D below1 {0, 0, -10};
    Point3D below2 {0, 0, -0.01};
    Point3D below3 {100, 0, -1};
    Point3D below4 {100, 100, -1};
    Point3D below5 {0, 100, -100};

    REQUIRE(get_point_position_on_face(below1, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below2, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below3, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below4, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below5, face) ==
            PointPosition::BelowFace);

    Point3D above1 {0, 0, 0.01};
    Point3D above2 {0, 0, 10};
    Point3D above3 {100, 0, 1};
    Point3D above4 {100, 100, 1};
    Point3D above5 {0, 100, 100};

    REQUIRE(get_point_position_on_face(above1, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above2, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above3, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above4, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above5, face) ==
            PointPosition::AboveFace);
  }

  {
    Face face {{-1279.97, -768, -1536},
               {-1280, -768.025, -1536},
               {-1024, -768, -1408}};

    Point3D on1 {-1279.97, -768, -1536};
    Point3D on2 {-1024, -768, -1408};
    Point3D on3 {-1280, -768, -1536.003};
    Point3D on4 {-1152, -896, -1395.20};
    Point3D on5 {-1024, -768, -1408};

    REQUIRE(get_point_position_on_face(on1, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on2, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on3, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on4, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on5, face) == PointPosition::OnFace);

    Point3D below1 {-1280, -1024, -1536};
    Point3D below2 {-1024, -768, -1409};
    Point3D below3 {-1157, -896, -1412};
    Point3D below4 {-1248, -992, -1412};
    Point3D below5 {-1280, -992, -1426};

    REQUIRE(get_point_position_on_face(below1, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below2, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below3, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below4, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below5, face) ==
            PointPosition::BelowFace);

    Point3D above1 {-1280, -1024, -1280};
    Point3D above2 {-1024, -768, -1280};
    Point3D above3 {-1152, -896, -1392};
    Point3D above4 {-1280, -768, -1535};
    Point3D above5 {-1216, -960, -1350};

    REQUIRE(get_point_position_on_face(above1, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above2, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above3, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above4, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above5, face) ==
            PointPosition::AboveFace);
  }

  {
    Face face {
        {-4096, -4096, -3584}, {-4096, -4096, -4096}, {-4096, -3584, -3584}};

    Point3D on1 {-4096, -4096, -4096};
    Point3D on2 {-4096, 4096, -4096};
    Point3D on3 {-4096, 4096, 4096};
    Point3D on4 {-4096, -3840, -3584};
    Point3D on5 {-4096, -3840, -4096};

    REQUIRE(get_point_position_on_face(on1, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on2, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on3, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on4, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on5, face) == PointPosition::OnFace);

    Point3D below1 {-4097, -4096, -4096};
    Point3D below2 {-4097, -4096, 4096};
    Point3D below3 {-4097, 4096, -4096};
    Point3D below4 {-4097, 0, -4096};
    Point3D below5 {-4097, 0, 0};

    REQUIRE(get_point_position_on_face(below1, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below2, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below3, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below4, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below5, face) ==
            PointPosition::BelowFace);

    Point3D above1 {0, 0, 0};
    Point3D above2 {-4095, -3584, 4096};
    Point3D above3 {-3584, -4096, -4096};
    Point3D above4 {-4095, -4096, -4096};
    Point3D above5 {-4095, -4096, 4096};

    REQUIRE(get_point_position_on_face(above1, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above2, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above3, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above4, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above5, face) ==
            PointPosition::AboveFace);
  }

  {
    Face face {{1152, -896, -768}, {1152, -896, -1280}, {896, -1152, -768}};

    Point3D on1 {1152, -896, -768};
    Point3D on2 {1151.995, -896, -768};
    Point3D on3 {1152.005, -896, -768};
    Point3D on4 {1024, -1024, 0};
    Point3D on5 {896, -1152, 512};

    REQUIRE(get_point_position_on_face(on1, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on2, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on3, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on4, face) == PointPosition::OnFace);
    REQUIRE(get_point_position_on_face(on5, face) == PointPosition::OnFace);

    Point3D below1 {1152, -1152, 0};
    Point3D below2 {896, -1153, 0};
    Point3D below3 {1153, -896, 0};
    Point3D below4 {1025, -1025, 0};
    Point3D below5 {1018, -1032, 0};

    REQUIRE(get_point_position_on_face(below1, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below2, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below3, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below4, face) ==
            PointPosition::BelowFace);
    REQUIRE(get_point_position_on_face(below5, face) ==
            PointPosition::BelowFace);

    Point3D above1 {896, -896, 0};
    Point3D above2 {1023, -1023, 0};
    Point3D above3 {1099, -948, 0};
    Point3D above4 {1151, -896, 0};
    Point3D above5 {768, -1152, 0};

    REQUIRE(get_point_position_on_face(above1, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above2, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above3, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above4, face) ==
            PointPosition::AboveFace);
    REQUIRE(get_point_position_on_face(above5, face) ==
            PointPosition::AboveFace);
  }
}

TEST_CASE("cross_product")
{
  {
    Point3D point1 {2, 1, 2};
    Point3D point2 {-2, -4, -2};

    Point3D result   = Point3D::cross_product(point1, point2);
    Point3D expected = {6, 0, -6};

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {1, 2, 3};
    Point3D point2 {4, 5, 30};

    Point3D result   = Point3D::cross_product(point1, point2);
    Point3D expected = {45, -18, -3};

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {-125.99, -190, 30.19};
    Point3D point2 {11, 39.93, -93.1};

    Point3D result   = Point3D::cross_product(point1, point2);
    Point3D expected = {16483.5133, -11397.579, -2940.7807};

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {0, 0, 0};
    Point3D point2 {0, 0, 0};

    Point3D result   = Point3D::cross_product(point1, point2);
    Point3D expected = {0, 0, 0};

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {0, -999, 0};
    Point3D point2 {8, -999, 0};

    Point3D result   = Point3D::cross_product(point1, point2);
    Point3D expected = {0, 0, 7992};

    REQUIRE(result == expected);
  }
}

TEST_CASE("dot_product")
{
  {
    Point3D point1 {0, 0, 0};
    Point3D point2 {0, 0, 0};

    double result   = Point3D::dot_product(point1, point2);
    double expected = 0;

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {1, 2, 3};
    Point3D point2 {4, 5, 30};

    double result   = Point3D::dot_product(point1, point2);
    double expected = 104;

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {-100, 200, -99};
    Point3D point2 {10, 29.99, -109};

    double result   = Point3D::dot_product(point1, point2);
    double expected = 15789;

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {3, 0, 9};
    Point3D point2 {8, -1, 2};

    double result   = Point3D::dot_product(point1, point2);
    double expected = 42;

    REQUIRE(result == expected);
  }

  {
    Point3D point1 {1, 1, 1};
    Point3D point2 {-1, -1, -1};

    double result   = Point3D::dot_product(point1, point2);
    double expected = -3;

    REQUIRE(result == expected);
  }
}

TEST_CASE("normalize")
{
  {
    Point3D point {-1, 2, 3};

    double resMagnitude = point.get_magnitude();
    double expMagnitude = 3.74165738677394;
    REQUIRE(std::abs(resMagnitude - expMagnitude) < Point3D::epsilon);

    Point3D resVector = point.normalize();
    Point3D expVector {
        -0.267261241912424, 0.534522483824849, 0.801783725737273};
    REQUIRE(resVector == expVector);
  }

  {
    Point3D point {9999, 9991, 9995};

    double resMagnitude = point.get_magnitude();
    double expMagnitude = 17311.8487458734;
    REQUIRE(std::abs(resMagnitude - expMagnitude) < Point3D::epsilon);

    Point3D resVector = point.normalize();
    Point3D expVector {0.577581293989957, 0.57711918274364, 0.577350238366799};
    REQUIRE(resVector == expVector);
  }

  {
    Point3D point {-913, 3789, 747};

    double resMagnitude = point.get_magnitude();
    double expMagnitude = 3968.3874558818;
    REQUIRE(std::abs(resMagnitude - expMagnitude) < Point3D::epsilon);

    Point3D resVector = point.normalize();
    Point3D expVector {
        -0.230068260760875, 0.954795881733797, 0.188237667895262};
    REQUIRE(resVector == expVector);
  }

  {
    Point3D point {85, 838, -93};

    double resMagnitude = point.get_magnitude();
    double expMagnitude = 847.41843265296;
    REQUIRE(std::abs(resMagnitude - expMagnitude) < Point3D::epsilon);

    Point3D resVector = point.normalize();
    Point3D expVector {0.100304639036344, 0.988885735440667, -0.10974507565153};
    REQUIRE(resVector == expVector);
  }

  {
    Point3D point {875, -84, 47734};

    double resMagnitude = point.get_magnitude();
    double expMagnitude = 47742.0929264732;
    REQUIRE(std::abs(resMagnitude - expMagnitude) < Point3D::epsilon);

    Point3D resVector = point.normalize();
    Point3D expVector {
        0.018327642262093, -0.00175945365716093, 0.999830486558567};
    REQUIRE(resVector == expVector);
  }
}
