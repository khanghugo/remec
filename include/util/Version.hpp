#pragma once

#include <string>
#include <string_view>

struct Version
{
  int major = 0;
  int minor = 0;
  int patch = 0;

  bool operator==(const Version &) const noexcept;
  bool operator>(const Version &) const noexcept;
  bool operator<(const Version &) const noexcept;
};

Version version_from_str(std::string_view sv);
std::string version_to_str(const Version &) noexcept;
