#pragma once

struct Point3D
{
  double x = 1.0;
  double y = 1.0;
  double z = 1.0;

  static constexpr double epsilon = 0.0001;
  static constexpr double offset_epsilon = 0.01;

  Point3D operator+(const Point3D &other) const noexcept;
  Point3D operator-(const Point3D &other) const noexcept;
  Point3D operator*(const double scalar) noexcept;

  Point3D &operator+=(const Point3D &other) noexcept;
  Point3D &operator*=(const double scalar) noexcept;
  Point3D &operator/=(const double scalar) noexcept;

  bool operator==(const Point3D &other) const noexcept;
  bool operator<(const Point3D &other) const noexcept;

  Point3D &normalize() noexcept;
  double get_magnitude() const noexcept;

  static Point3D cross_product(const Point3D &point1, const Point3D &point2) noexcept;
  static double dot_product(const Point3D &point1, const Point3D &point2) noexcept;
};
