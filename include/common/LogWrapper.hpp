#pragma once

#include "Utils.hpp"

#include <fmt/color.h>
#include <iostream>
#include <source_location>
#include <spdlog/spdlog.h>

extern spdlog::logger *g_logger;

enum class Level
{
  Info,
  Error,
  Warn,
  Debug,
};

using Color = fmt::terminal_color;

template <Color color, typename T>
auto asColor(T &&value) noexcept
{
#ifdef _WIN32
  constexpr auto isWin32 = true;
#else
  constexpr auto isWin32 = false;
#endif

  if constexpr (isWin32) {
    return fmt::format("{}", std::forward<T>(value));
  } else {
    if (not isAtty()) {
      return fmt::format("{}", std::forward<T>(value));
    } else {
      return fmt::format(fmt::fg(color), "{}", std::forward<T>(value));
    }
  }
}

static const std::string BasePath = BASE_PATH;

template <typename... Args>
struct logging
{
  logging(Level level,
          const std::string &fmtString,
          Args &&...args,
          const std::source_location &loc =
              std::source_location::current()) noexcept
  {
    if (not g_logger) {
      return;
    }

    switch (level) {
    case Level::Info:
      g_logger->info(
          fmt::runtime("[{}:{}]: " + fmtString),
          asColor<Color::green>(loc.file_name() + BasePath.length() + 1),
          asColor<Color::green>(loc.line()),
          std::forward<Args>(args)...);
      break;

    case Level::Error:
      g_logger->error(
          fmt::runtime("[{}:{}]: " + fmtString),
          asColor<Color::green>(loc.file_name() + BasePath.length() + 1),
          asColor<Color::green>(loc.line()),
          std::forward<Args>(args)...);
      break;

    case Level::Warn:
      g_logger->warn(
          fmt::runtime("[{}:{}]: " + fmtString),
          asColor<Color::green>(loc.file_name() + BasePath.length() + 1),
          asColor<Color::green>(loc.line()),
          std::forward<Args>(args)...);
      break;

    case Level::Debug:
      g_logger->debug(
          fmt::runtime("[{}:{}]: " + fmtString),
          asColor<Color::green>(loc.file_name() + BasePath.length() + 1),
          asColor<Color::green>(loc.line()),
          std::forward<Args>(args)...);
      break;
    }
  }
};

template <typename... Args>
logging(Level level, const std::string &fmtString, Args &&...args)
    -> logging<Args...>;
