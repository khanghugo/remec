#pragma once

#include <string>
#include <unordered_map>

using PropertiesMap = std::unordered_map<std::string, std::string>;
