#include <common/Face.hpp>

Face::Face(const Point3D &p1, const Point3D &p2, const Point3D &p3)
    : point1 {p1}
    , point2 {p2}
    , point3 {p3}
{
}

bool Face::operator==(const Face &other) const noexcept
{
  return point1 == other.point1 && point2 == other.point2 &&
         point3 == other.point3;
}

Point3D &Face::get_normal_vector() const noexcept
{
  if (!normal.has_value())
    initialize_normal_vector();

  return *normal;
}

void Face::initialize_normal_vector() const noexcept
{
  normal.emplace(
      Point3D::cross_product((point2 - point1), (point3 - point1)).normalize());
}

double Face::get_face_offset() const noexcept
{
  if (!offset.has_value())
    offset = Point3D::dot_product(get_normal_vector(), point1);

  return *offset;
}

std::optional<Point3D> get_intersection_point(const Face &face1,
                                              const Face &face2,
                                              const Face &face3) noexcept
{
  // https://web.archive.org/web/20120314001459/http://paulbourke.net/geometry/3planes/
  Point3D p1 = Point3D::cross_product(face2.get_normal_vector(),
                                      face3.get_normal_vector());

  const double denominator =
      Point3D::dot_product(face1.get_normal_vector(), p1);
  if (denominator < Point3D::epsilon)
    return std::nullopt; // intersection is not a point

  Point3D p2 = Point3D::cross_product(face3.get_normal_vector(),
                                      face1.get_normal_vector());
  Point3D p3 = Point3D::cross_product(face1.get_normal_vector(),
                                      face2.get_normal_vector());

  p1 *= face1.get_face_offset();
  p2 *= face2.get_face_offset();
  p3 *= face3.get_face_offset();
  Point3D numerator = p1 + p2 + p3;

  return (numerator /= denominator);
}

PointPosition get_point_position_on_face(const Point3D &point,
                                         const Face &face) noexcept
{
  const double result = point.x * face.get_normal_vector().x +
                        point.y * face.get_normal_vector().y +
                        point.z * face.get_normal_vector().z -
                        face.get_face_offset();

  if (std::abs(result) < Point3D::offset_epsilon) {
    return PointPosition::OnFace;
  } else if (result < 0) {
    return PointPosition::BelowFace;
  } else {
    return PointPosition::AboveFace;
  }
}
