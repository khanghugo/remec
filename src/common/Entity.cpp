#include <algorithm>
#include <common/Entity.hpp>
#include <fmt/format.h>
#include <stdexcept>

spdlog::logger *g_logger = nullptr;
ITracer *g_tracer        = nullptr;

Entity::Entity(Context context,
               std::size_t originalStartPos,
               std::size_t originalLen,
               std::unordered_map<std::string, std::string> properties,
               std::vector<Brush> brushes)
    : m_context {std::move(context)}
    , m_originalStartPos {originalStartPos}
    , m_originalLen {originalLen}
    , m_properties {std::move(properties)}
    , m_brushes {std::move(brushes)}
{
  g_logger = m_context.logger;
  g_tracer = m_context.tracer;
}

std::size_t Entity::getOriginalStartPos() const noexcept
{
  return m_originalStartPos;
}

std::size_t Entity::getOriginalLen() const noexcept
{
  return m_originalLen;
}

std::string Entity::getName() const noexcept
{
  try {
    return m_properties.at("targetname");
  } catch (...) {
    return {};
  }
}

std::vector<Brush> Entity::getBrushes() const noexcept
{
  return m_brushes;
}

const PropertiesMap &Entity::getProperties() const
{
  return m_properties;
}

std::string Entity::getStringProperty(const std::string &name) const
{
  const auto &props = getProperties();

  if (not props.count(name)) {
    throw std::runtime_error {fmt::format(
        "Missing '{}' from entity '{}' !", name, getEntityClassName())};
  }

  const auto property = props.at(name);

  if (property.empty()) {
    throw std::runtime_error {fmt::format(
        "'{}' on entity '{}' cannot be empty !", name, getEntityClassName())};
  }

  return property;
}

double Entity::getDoubleProperty(const std::string &name) const
{
  return std::stod(getStringProperty(name));
}

int Entity::getIntProperty(const std::string &name) const
{

  return std::stoi(getStringProperty(name));
}

PropertiesMap Entity::getFilteredProperties() const noexcept
{
  PropertiesMap result;

  const auto &properties   = getProperties();
  const auto ownProperties = getOwnProperties();

  for (const auto &[key, value] : properties) {
    // trenchbroom properties start with _, we don't care about them
    if (key != "targetname" and key != "classname" and key[0] != '_' and
        std::find_if(std::begin(*ownProperties),
                     std::end(*ownProperties),
                     [key = key](const auto &ownProperty) {
                       return ownProperty.name == key;
                     }) == std::end(*ownProperties)) {
      result[key] = value;
    }
  }

  return result;
}

const OwnProperty &Entity::getOwnProperty(const std::string &name) const
{
  const auto ownProps = getOwnProperties();

  const auto it = std::find_if(
      std::begin(*ownProps), std::end(*ownProps), [&name](const auto &prop) {
        return prop.name == name;
      });

  if (it != std::end(*ownProps)) {
    return *it;
  }

  throw std::runtime_error {fmt::format("Property '{}' not found !", name)};
}

const Context &Entity::getContext() const noexcept
{
  return m_context;
}