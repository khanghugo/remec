#include "common/LogWrapper.hpp"
#include "tracing/ITracer.hpp"

#include <PluginDiscovery.hpp>
#include <Utils.hpp>

std::vector<std::filesystem::path>
discover_plugins(const std::filesystem::path &pluginsDirPath)
{
  REMEC_TRACE(Category::remec_plugin_discovery, "discover_plugins");

  namespace fs = std::filesystem;

  logging(Level::Debug,
          "Discovering plugins in '{}'",
          asColor<Color::blue>(pluginsDirPath.string()));

  std::vector<std::filesystem::path> results;

  for (const auto &entry : fs::directory_iterator(pluginsDirPath)) {
    if (not entry.is_regular_file()) {
      continue;
    }

    const auto &path = entry.path();

    if (path.filename().extension() == SharedLibraryExtension()) {
      results.push_back(path);

      logging(Level::Debug,
              "Discovered '{}'",
              asColor<Color::green>(path.string()));
    }
  }

  if (results.empty()) {
    throw std::runtime_error {
        fmt::format("No plugins found in '{}'", pluginsDirPath.string())};
  }

  return results;
}
