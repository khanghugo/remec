#include "Rewriter.hpp"

#include "tracing/ITracer.hpp"

#include <common/Entity.hpp>
#include <fmt/format.h>

Rewriter::Rewriter(std::string_view original)
    : m_original {original}
{
}

std::string
Rewriter::rewrite(const std::vector<IPluginSmartPtr> &entities) const
{
  REMEC_TRACE(Category::remec_rewriter, "rewrite");

  std::size_t offset = 0u;

  auto result = std::string {m_original};

  for (const auto &entity : entities) {
    if (not entity) {
      continue;
    }

    REMEC_TRACE(Category::remec_rewriter, entity->getEntityClassName());

    const auto serialized = entity->serialize();

    if (not serialized) {
      // can't throw exceptions between shared library boundries (at least on windows)
      // so we throw the exception here instead
      throw std::runtime_error {serialized.error()};
    }

    const auto entityStr =
        fmt::format("// <created by REMEC from {source_ent}>\n{ent}\n// "
                    "</created by REMEC from {source_ent}>",
                    fmt::arg("source_ent", entity->getEntityClassName()),
                    fmt::arg("ent", serialized.value()));

    result.replace(entity->getOriginalStartPos() + offset,
                   entity->getOriginalLen() + 1,
                   entityStr + '\n');

    offset += entityStr.length() - entity->getOriginalLen();
  }

  return result;
}
