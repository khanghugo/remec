#pragma once

#include <common/CustomEntity.hpp>

class TriggerRandomTarget final : public CustomEntity<TriggerRandomTarget> {
public:
  using CustomEntity<TriggerRandomTarget>::CustomEntity;

  static constexpr inline auto ClassName = "remec_trigger_random_target";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "Trigger a random target from a list";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = false;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
