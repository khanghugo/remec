# LitModel

A `cycler_sprite` or any model displaying entity of your choice lit by a light emitting `func_detail`

If entity is `env_sprite`, model's angles will be adjusted to non-zero so it can be displayed in-game.

## Spawnflags

`1` will lit up the model.

`2` will correct Source model entity angle (`prop_static`) when a simple `classname` change is made.

`4` will make the entity rotate accordingly to `avelocity` values. Preferrably `entity` must be `env_sprite` to avoid entity duplication.

`8` will fix the Y in (Y Z X) of entity angle so the model appears like what the editor shows.

`16` will use `targetname` for the entity. The reason why it is an option is that a plain `env_sprite` with a `targetname` will not show up in the game. Also, REMEC would generate a pseudoname for entities if appropriate, especially when flag `4` (rotating) is marked and entity must be named to work. 

**Note**: You must create a `light_surface` or `info_texlights` to make your chosen `texture` emit light.

## Properties

| Property        | Default value        | Description                                            | Possible values                         |
| --------------- | -------------------- | ------------------------------------------------------ | --------------------------------------- |
| `texture`       | `black_HIDDEN`       | Light emitting texture                                 |                                         |
| `entity`        | `cycler_sprite`      | Displaying entity                                      | `cycler_sprite`, `env_sprite`, `cycler` |
| `model`         |                      | Path to model                                          |                                         |
| `spawnflags`    | `1`                  | Options for the entity                                 | `1`: Lit <br> `2`: Source `prop_static` <br> `4`: Rotating <br> `8`: Fix Y in (Y Z X) <br> `16`: Use `targetname`                               |
| `targetname`    |                      | Entity targetname                                      |                                         |
| `avelocity`     | `0 0 0`              | Angular velocity if rotating                           |                                         |

## Generates

spawnflags `0` (no options):
```mermaid
graph TD;

subgraph remec_lit_model
    CS["cycler_sprite"]
end
```

spawnflags `1` (lit):
```mermaid
graph TD;

subgraph remec_lit_model
    CS["cycler_sprite"]
    FD["func_detail"]
end
```

spawnflags `4` (rotating):
```mermaid
graph TD;

subgraph remec_lit_model
    CS["cycler_sprite"]
    FR["func_train"]
    PC1["path_corner"]
    PC2["path_corner"]

    FR --> CS
    FR --> PC1
    PC1 --> PC2
    PC2 --> PC1
end
```