#include "LitModel.hpp"

#include <Utils.hpp>
#include <bitset>
#include <fmt/format.h>

namespace {
std::vector<Brush> adjust_brushes(std::vector<Brush> brushesToAdjust,
                                  const Point3D &relativeTo)
{
  if (brushesToAdjust.empty()) {
    throw std::runtime_error {"Adjusting failed! No brushes provided!"};
  }

  for (auto &brush : brushesToAdjust) {
    for (auto i = 0u; i != brush.size(); ++i) {
      auto &adjustedFace = brush[i];

      adjustedFace.point1 += relativeTo;
      adjustedFace.point2 += relativeTo;
      adjustedFace.point3 += relativeTo;
    }
  }

  return brushesToAdjust;
}
} // namespace

tl::expected<std::string, std::string> LitModel::serialize() const
{
  try {
    const auto model      = getOrDefault<std::string>("model");
    const auto entity     = getOrDefault<std::string>("entity");
    const auto texture    = getOrDefault<std::string>("texture");
    const auto targetname = getOrDefault<std::string>("targetname");
    const auto avelocity  = getOrDefault<std::string>("avelocity");

    const auto props  = getFilteredProperties();
    const auto origin = getOrDefault<std::string>("origin");
    const auto angles = getOrDefault<std::string>("angles");

    const auto spawnflagsBits = std::bitset<6> {
        static_cast<std::uint32_t>(getOrDefault<int>("spawnflags"))};
    const auto should_lit            = spawnflagsBits[0];
    const auto is_source             = spawnflagsBits[1];
    const auto should_rotate         = spawnflagsBits[2];
    const auto fix_y                 = spawnflagsBits[3];
    const auto should_use_targetname = spawnflagsBits[4];

    // Brush is at the origin.
    const auto litBrush =
        brushes_from_str(fmt::format(R"(
{{
( -8 -120 -24 ) ( -8 -119 -24 ) ( -8 -120 -23 ) SKIP [ 0 -1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -120 -8 -24 ) ( -120 -8 -23 ) ( -119 -8 -24 ) SKIP [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -120 -120 -8 ) ( -119 -120 -8 ) ( -120 -119 -8 ) SKIP [ -1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 8 8 8 ) ( 8 9 8 ) ( 9 8 8 ) {litTexture} [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 8 8 8 ) ( 9 8 8 ) ( 8 8 9 ) SKIP [ -1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 8 8 8 ) ( 8 8 9 ) ( 8 9 8 ) SKIP [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
}}
)",
                                     fmt::arg("litTexture", texture)));

    const auto rotatingBrush = brushes_from_str(R"(
{
( -8 -120 -24 ) ( -8 -119 -24 ) ( -8 -120 -23 ) AAATRIGGER [ 0 -1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -120 -8 -24 ) ( -120 -8 -23 ) ( -119 -8 -24 ) AAATRIGGER [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -120 -120 -8 ) ( -119 -120 -8 ) ( -120 -119 -8 ) AAATRIGGER [ -1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 8 8 8 ) ( 8 9 8 ) ( 9 8 8 ) AAATRIGGER [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 8 8 8 ) ( 9 8 8 ) ( 8 8 9 ) AAATRIGGER [ -1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 8 8 8 ) ( 8 8 9 ) ( 8 9 8 ) AAATRIGGER [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
}
)");

    const auto relativeTo = origin_from_str(origin);
    auto adjustedAngles   = origin_from_str(angles);

    if (is_source) {
      adjustedAngles.y += 90.0;
    }

    const auto is_env_sprite = entity == "env_sprite";

    // fix env_sprite Z and X in (Y Z X) flip due to Z != 0 and X == 0
    if (is_env_sprite) {
      // https://github.com/ValveSoftware/halflife/blob/c7240b965743a53a29491dd49320c88eecf6257b/dlls/effects.cpp#L1199-L1203
      if (adjustedAngles.z == 0.0) {
        static const constexpr auto Epsilon = 0.00001;

        adjustedAngles.z = Epsilon;
      }
    }

    // Originally Y Z X having Y flipped so editor looks different from in-game.
    if (fix_y)
      adjustedAngles.x *= -1.0;

    const auto litBrushStr = fmt::format(
        R"(
{{
{layer}"classname" "func_detail"
"zhlt_noclip" "1"
{litBrush}
}}
)",
        // Need to offset relativeTo by -1u in z axis because it will surely be below the entity.
        // This is for an edge case where point entity has some decimal places so that the brush
        // might possibly be above the entity.
        fmt::arg("litBrush",
                 brushes_to_str(
                     adjust_brushes(litBrush, relativeTo + Point3D(0, 0, -1)))),
        fmt::arg("layer", getLayer()));

    // Purely for rotating models.
    // Generate new pseudo name.
    // For ease of use because keep setting targetname is not very fun.
    const std::string pseudo_usemodel_name =
        std::to_string(relativeTo.get_magnitude() + relativeTo.x +
                       relativeTo.y + relativeTo.z);

    // If rotating and no "use targetname", use pseudo name.
    // If rotating and targetname field is empty, use pseudo name.
    // If rotating and targetname field is not empty, use targetname.
    // If use targetname, use targetname or pseudo name.
    const auto _model_targetname =
        targetname.length() ? targetname : pseudo_usemodel_name;
    const auto model_targetname =
        should_rotate || should_use_targetname ? _model_targetname : "";

    const auto rotatingBrushStr = fmt::format(
        R"(
{{
{layer}"classname" "func_train"
"zhlt_usemodel" "{targetname}"
"speed" "0.001"
"avelocity" "{avelocity}"
"spawnflags" "8"
"target" "{targetname}_p1"
"angles" "{angles}"
{rotatingBrush}
}}

{{
{layer}"classname" "path_corner"
"origin" "{origin1}"
"targetname" "{targetname}_p1"
"target" "{targetname}_p2"
}}

{{
{layer}"classname" "path_corner"
"origin" "{origin2}"
"targetname" "{targetname}_p2"
"target" "{targetname}_p1"
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("targetname", model_targetname),
        fmt::arg("avelocity", avelocity),
        fmt::arg("rotatingBrush",
                 brushes_to_str(adjust_brushes(rotatingBrush, relativeTo))),
        fmt::arg("origin1", origin),
        // If two path_corner have the same origin then game will crash.
        fmt::arg("origin2",
                 origin_to_str(origin_from_str(origin) + Point3D(0, 0, 0.001))),
        // func_train loads model with zhlt_usemodel differently and we need to specify angle.
        fmt::arg("angles", origin_to_str(adjustedAngles)));

    return fmt::format(
        R"(
{{
{layer}"classname" "{entity}"
"targetname" "{targetname}"
"origin" "{origin}"
"model" "{model}"
"angles" "{angles}"
}}

{litBrush}
{rotatingBrush}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("entity", entity),
        fmt::arg("targetname", model_targetname),
        fmt::arg("origin", origin),
        fmt::arg("model", model),
        fmt::arg("angles", origin_to_str(adjustedAngles)),
        fmt::arg("litBrush", should_lit ? litBrushStr : ""),
        fmt::arg("rotatingBrush", should_rotate ? rotatingBrushStr : ""));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &LitModel::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {"targetname",
                   "",
                   "targetname for entity",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {
          "origin", "0 0 0", "Entity origin", OwnProperty::Type::String, {}},
      OwnProperty {"angles",
                   "0 0 0",
                   "Entity angles (Y Z X)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"model", "", "Path to model", OwnProperty::Type::String, {}},
      OwnProperty {"texture",
                   "black_HIDDEN",
                   "Light emitting texture",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"entity",
                   "cycler_sprite",
                   "Displaying entity",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"spawnflags",
                   9,
                   "Spawnflags",
                   OwnProperty::Type::Flags,
                   {
                       {1, "Lit", 1},
                       {2, "Source prop_static", 0},
                       {4, "Rotating", 0},
                       {8, "Fix Y in (Y Z X)", 1},
                       {16, "Use targetname", 0},
                   }},
      OwnProperty {"avelocity",
                   "0 0 0",
                   "Angular velocity if rotating",
                   OwnProperty::Type::String,
                   {}}};

  return props;
}

REGISTER_PLUGIN(LitModel)
