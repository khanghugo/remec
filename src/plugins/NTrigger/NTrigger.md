# NTrigger

Similar to `trigger_counter`, this entity can be triggered multiple (`count`) times after which it will trigger the `target`.

Unlike `trigger_counter`, you can set up this entity to trigger different targets for each iteration.

You can also trigger this entity to reset the counter.

## Properties

| Property     | Default value | Description                                            |
| ------------ | ------------- | ------------------------------------------------------ |
| `targetname` |               | Name                                                   |
| `target`     |               | Target to trigger                                      |
| `delay`      | `0.0`         | Delay before actually triggering                       |
| `count`      | `2`           | After how many triggers to actually trigger the target |
| `0`          |               | Target to trigger after 1st trigger as value           |
| `1`          |               | Target to trigger after 2nd trigger as value           |
| ....         |               | ...                                                    |

## Generates

```mermaid
graph TD;

TGR["external trigger"]
TGT["target"]

ITTGT0["1st trigger target"]
ITTGT1["2nd trigger target"]
ITTGT2["3rd trigger target"]

subgraph remec_n_trigger
  TM["trigger_multiple"]
  style TM stroke:violet,color:violet;
  MM0["multi_manager 0"]
  style MM0 stroke:orange,color:orange;
  MM1["multi_manager 1"]
  style MM1 stroke:orange,color:orange;
  MM2["multi_manager 2"]
  style MM2 stroke:orange,color:orange;
  TM_CT0["trigger_changetarget"]
  style TM_CT0 stroke:magenta,color:magenta;
  TM_CT1["trigger_changetarget"]
  style TM_CT1 stroke:magenta,color:magenta;
  TM_CT2["trigger_changetarget"]
  style TM_CT2 stroke:magenta,color:magenta;

  MM0 --> TM_CT0
  linkStyle 0 stroke:orange;
  MM1 --> TM_CT1
  linkStyle 1 stroke:orange;
  MM2 --> TM_CT2
  linkStyle 2 stroke:orange;

  TM_CT0 --sets MM 1--> TM
  linkStyle 3 stroke:magenta;
  TM_CT1 --sets MM 2--> TM
  linkStyle 4 stroke:magenta;
  TM_CT2 --sets MM 0--> TM
  linkStyle 5 stroke:magenta;
end

TGR -----> TM

MM0 -...-> ITTGT0
linkStyle 7 stroke:orange,stroke-dasharray:10;
MM1 -...-> ITTGT1
linkStyle 8 stroke:orange,stroke-dasharray:10;
MM2 -...-> ITTGT2
linkStyle 9 stroke:orange,stroke-dasharray:10;

MM2 -----> TGT
linkStyle 10 stroke:orange;
```
