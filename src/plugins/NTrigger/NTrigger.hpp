#pragma once

#include <common/CustomEntity.hpp>

class NTrigger final : public CustomEntity<NTrigger> {
public:
  using CustomEntity<NTrigger>::CustomEntity;

  static constexpr inline auto ClassName = "remec_n_trigger";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "Trigger target only after N triggers";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
