#pragma once

#include <common/CustomEntity.hpp>

class AutoResetWall final : public CustomEntity<AutoResetWall> {
public:
  using CustomEntity<AutoResetWall>::CustomEntity;

  static constexpr inline auto ClassName   = "remec_auto_reset_wall";
  static constexpr inline auto Version     = "2.0.0";
  static constexpr inline auto Description = "Triggarable auto-resettable wall";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
