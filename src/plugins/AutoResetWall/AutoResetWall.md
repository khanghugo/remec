# AutoResetWall

A wall brush entity that when triggered will automatically trigger again after a delay.

## Properties

| Property          | Default value | Description                 | Possible values                                                                                                                                                                                                                                                                                                                                                                                  |
| ----------------- | ------------- | --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `targetname`      |               | Name                        |                                                                                                                                                                                                                                                                                                                                                                                                  |
| `delay`           | `0.0`         | Delay before first trigger  |                                                                                                                                                                                                                                                                                                                                                                                                  |
| `delay2`          | `1.0`         | Delay before second trigger |                                                                                                                                                                                                                                                                                                                                                                                                  |
| `spawnflags`      | `0`           | Spawn flags                 | <ul><li>Starts invisible</li><li>Not in Deathmatch</li></ul>                                                                                                                                                                                                                                                                                                                                     |
| `globalname`      |               | Global entity name          |                                                                                                                                                                                                                                                                                                                                                                                                  |
| `_minlight`       |               | Minimum light level         |                                                                                                                                                                                                                                                                                                                                                                                                  |
| `rendermode`      | `0`           | Render Mode                 | <ul><li>Normal</li><li>Color</li><li>Texture</li><li>Glow</li><li>Solid</li><li>Additive</li></ul>                                                                                                                                                                                                                                                                                               |
| `renderfx`        | `0`           | Render FX                   | <ul><li>Normal</li><li>Slow Pulse</li><li>Fast Pulse</li><li>Slow Wide Pulse</li><li>Fast Wide Pulse</li><li>Slow Fade Away</li><li>Fast Fade Away</li><li>Slow Become Solid</li><li>Fast Become Solid</li><li>Slow Strobe</li><li>Fast Strobe</li><li>Faster Strobe</li><li>Slow Flicker</li><li>Fast Flicker</li><li>Constant Glow</li><li>Distort</li><li>Hologram (Distort + fade)</li></ul> |
| `renderamt`       |               | FX Amount                   | `0-255`                                                                                                                                                                                                                                                                                                                                                                                          |
| `rendercolor`     | `0 0 0`       | FX Color                    | `0-255` `0-255` `0-255`                                                                                                                                                                                                                                                                                                                                                                          |
| `zhlt_lightflags` | `0`           | ZHLT Lightflags             | <ul><li>Default</li><li>Embedded Fix</li><li>Opaque (blocks light)</li><li>Opaque + Embedded fix</li><li>Opaque + Concave Fix</li></ul>                                                                                                                                                                                                                                                          |
| `light_origin`    |               | Light origin target         |                                                                                                                                                                                                                                                                                                                                                                                                  |

## Generates

```mermaid
graph TD;

TRG["external trigger"]

subgraph remec_auto_reset_wall
  FWT["func_wall_toggle"]
  style FWT stroke:lightblue,color:lightblue;
  MM1["multi_manager"]
  style MM1 stroke:orange,color:orange;
  MM2["multi_manager"]
  style MM2 stroke:orange,color:orange;

  MM1 --delay--> FWT
  linkStyle 0 fill:none,stroke:orange;
  MM1 --delay--> MM2
  linkStyle 1 fill:none,stroke:orange;

  MM2 --delay2--> FWT
  linkStyle 2 fill:none,stroke:orange;
end

TRG --> MM1
```
