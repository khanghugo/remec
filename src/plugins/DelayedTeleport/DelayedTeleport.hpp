#pragma once

#include <common/CustomEntity.hpp>

class DelayedTeleport final : public CustomEntity<DelayedTeleport> {
public:
  using CustomEntity<DelayedTeleport>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_delayed_teleport";
  static constexpr inline auto Version             = "2.0.0";
  static constexpr inline auto Description         = "Teleport after a delay";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
