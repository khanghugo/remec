#include "DelayedTeleport.hpp"

#include <PluginDiscovery.hpp>
#include <PluginEntityFactoryImpl.hpp>
#include <SharedLibraryUtils.hpp>
#include <Utils.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

class DelayedTeleportFixture {
public:
  DelayedTeleportFixture()
  {
    const auto discoveredPluginsPaths = discover_plugins(REMEC_BINARY_DIR);

    m_entityFactory = std::make_unique<PluginEntityFactoryImpl>(
        "", discoveredPluginsPaths, "");

    auto lib =
        m_entityFactory->getLibraryForClassName(DelayedTeleport::ClassName);

    newInstanceFunc = get_library_function_T<newInstance_t>(lib, newInstance_s);
    deleteInstanceFunc =
        get_library_function_T<deleteInstance_t>(lib, deleteInstance_s);
  }

  std::unique_ptr<PluginEntityFactoryImpl> m_entityFactory;
  newInstance_t newInstanceFunc       = nullptr;
  deleteInstance_t deleteInstanceFunc = nullptr;
};

TEST_CASE_METHOD(DelayedTeleportFixture, "TestDelayedTeleport")
{
  SECTION("getters")
  {
    const auto ars = newInstanceFunc(
        Context {},
        0,
        1,
        {{"classname", DelayedTeleport::ClassName}, {"targetname", "somename"}},
        {});

    REQUIRE(ars->getEntityClassName() ==
            std::string(DelayedTeleport::ClassName));
    REQUIRE(ars->getName() == "somename");
    REQUIRE(ars->getOriginalStartPos() == 0);
    REQUIRE(ars->getOriginalLen() == 1);
    REQUIRE(ars->getProperties().size() == 2);
    REQUIRE(ars->getBrushes().size() == 0);

    deleteInstanceFunc(ars);
  }

  SECTION("serialize")
  {
    const auto ars =
        newInstanceFunc(Context {},
                        0,
                        1,
                        {
                            {"targetname", "somename"},
                            {"classname", DelayedTeleport::ClassName},
                            {"delay", "0"},
                            {"target", "sometarget"},
                        },
                        brushes_from_str(R"({
( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) some_texture [ 0 -1 0 50] [ 0 0 -1 -4 ] 0 1 1
( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) some_texture [ 1 0 0 27 ] [ 0 0 -1 -4 ] 0 1 1
( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) some_texture [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) some_texture [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) some_texture [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) some_texture [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
})"));

    const std::string expected = R"(
// trigger_multiple triggers somename_mm instantly
{
"classname" "trigger_multiple"
"targetname" "somename"
"target" "somename_mm"
{
( -134.000000 -41.000000 -52.000000 ) ( -134.000000 -40.000000 -52.000000 ) ( -134.000000 -41.000000 -51.000000 ) AAATRIGGER [ 0.000000 -1.000000 0.000000 50.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -148.000000 -28.000000 -52.000000 ) ( -148.000000 -28.000000 -51.000000 ) ( -147.000000 -28.000000 -52.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -153.000000 -41.000000 -26.000000 ) ( -152.000000 -41.000000 -26.000000 ) ( -153.000000 -40.000000 -26.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -32.307175 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 44.000000 -11.000000 ) ( -93.000000 45.000000 -11.000000 ) ( -92.000000 44.000000 -11.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 -17.000000 -44.000000 ) ( -92.000000 -17.000000 -44.000000 ) ( -93.000000 -17.000000 -43.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -27.000000 ] [ 0.000000 0.000000 -1.000000 2.200476 ] 0.000000 1.000000 1.000000
( -120.000000 44.000000 -49.000000 ) ( -120.000000 44.000000 -48.000000 ) ( -120.000000 45.000000 -49.000000 ) AAATRIGGER [ 0.000000 1.000000 0.000000 -50.000000 ] [ 0.000000 0.000000 -1.000000 -2.858852 ] 0.000000 1.000000 1.000000
}

}

// manager triggers somename_door after 0.000000 seconds
{
"classname" "multi_manager"
"origin" "-127.000000 -22.500000 -18.500000"
"targetname" "somename_mm"
"spawnflags" "0"
"somename_door" "0.000000"
}

// door triggers somename_ms instantly enabling it and disables it after 1 second
{
"classname" "func_door"
"targetname" "somename_door"
"wait" "1"
"delay" "0"
"lip" "0"
"speed" "0"
"spawnflags" "9"
"target" "somename_ms"
"angles" "90 0 0"
{
( -134.000000 -41.000000 -52.000000 ) ( -134.000000 -40.000000 -52.000000 ) ( -134.000000 -41.000000 -51.000000 ) AAATRIGGER [ 0.000000 -1.000000 0.000000 50.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -148.000000 -28.000000 -52.000000 ) ( -148.000000 -28.000000 -51.000000 ) ( -147.000000 -28.000000 -52.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -153.000000 -41.000000 -26.000000 ) ( -152.000000 -41.000000 -26.000000 ) ( -153.000000 -40.000000 -26.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -32.307175 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 44.000000 -11.000000 ) ( -93.000000 45.000000 -11.000000 ) ( -92.000000 44.000000 -11.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 -17.000000 -44.000000 ) ( -92.000000 -17.000000 -44.000000 ) ( -93.000000 -17.000000 -43.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -27.000000 ] [ 0.000000 0.000000 -1.000000 2.200476 ] 0.000000 1.000000 1.000000
( -120.000000 44.000000 -49.000000 ) ( -120.000000 44.000000 -48.000000 ) ( -120.000000 45.000000 -49.000000 ) AAATRIGGER [ 0.000000 1.000000 0.000000 -50.000000 ] [ 0.000000 0.000000 -1.000000 -2.858852 ] 0.000000 1.000000 1.000000
}

}

// multisource is the master of trigger_teleport
{
"classname" "multisource"
"origin" "-127.000000 -22.500000 -18.500000"
"targetname" "somename_ms"
}

// when somename_door is "open" the teleport is enabled
{
"classname" "trigger_teleport"
"target" "sometarget"
"spawnflags" "0"
"master" "somename_ms"
{
( -134.000000 -41.000000 -52.000000 ) ( -134.000000 -40.000000 -52.000000 ) ( -134.000000 -41.000000 -51.000000 ) AAATRIGGER [ 0.000000 -1.000000 0.000000 50.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -148.000000 -28.000000 -52.000000 ) ( -148.000000 -28.000000 -51.000000 ) ( -147.000000 -28.000000 -52.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -153.000000 -41.000000 -26.000000 ) ( -152.000000 -41.000000 -26.000000 ) ( -153.000000 -40.000000 -26.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -32.307175 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 44.000000 -11.000000 ) ( -93.000000 45.000000 -11.000000 ) ( -92.000000 44.000000 -11.000000 ) AAATRIGGER [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 -17.000000 -44.000000 ) ( -92.000000 -17.000000 -44.000000 ) ( -93.000000 -17.000000 -43.000000 ) AAATRIGGER [ -1.000000 0.000000 0.000000 -27.000000 ] [ 0.000000 0.000000 -1.000000 2.200476 ] 0.000000 1.000000 1.000000
( -120.000000 44.000000 -49.000000 ) ( -120.000000 44.000000 -48.000000 ) ( -120.000000 45.000000 -49.000000 ) AAATRIGGER [ 0.000000 1.000000 0.000000 -50.000000 ] [ 0.000000 0.000000 -1.000000 -2.858852 ] 0.000000 1.000000 1.000000
}

}
)";

    const auto outStr        = ars->serialize().value();
    const auto output        = split(outStr, '\n');
    const auto expectedSplit = split(expected, '\n');

    REQUIRE(output.size() == expectedSplit.size());

    for (auto i = 0u; i != output.size(); ++i) {
      REQUIRE(output[i] == expectedSplit[i]);
    }

    deleteInstanceFunc(ars);
  }
}
