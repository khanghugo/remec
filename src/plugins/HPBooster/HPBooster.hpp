#pragma once

#include <common/CustomEntity.hpp>

class HPBooster final : public CustomEntity<HPBooster> {
public:
  using CustomEntity<HPBooster>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_hp_booster";
  static constexpr inline auto Version             = "2.0.0";
  static constexpr inline auto Description         = "Infinite HP Booster";
  static constexpr inline auto TargetNameMandatory = false;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
