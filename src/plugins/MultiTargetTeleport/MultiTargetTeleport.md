# MultiTargetTeleport

Teleport target to several destinations.

Destinations are iterated over in **alphabetical order**.

Triggering this entity via `trigger` will change the teleport destination to the next one in the list, if already at last destination, loop-back to first destination.

## Properties

| Property     | Default value | Description                                            |
| ------------ | ------------- | ------------------------------------------------------ |
| `targetname` |               | Name                                                   |
| `trigger`    |               | Button/trigger that should change the teleport target  |
| `target1`    |               | First teleport destination; value is delay in seconds  |
| `target2`    |               | Second teleport destination; value is delay in seconds |
| `target3`    |               | ...                                                    |
| ....         |               | ...                                                    |

## Generates

```mermaid
graph TD;

TGR["trigger"]
TGT1["target1"]
TGT2["target2"]
TGT3["target3"]

subgraph remec_multi_target_teleport
  TT["trigger_teleport"]
  style TT stroke:lime,color:lime;

  TCT1["trigger_changetarget"]
  style TCT1 stroke:magenta,color:magenta;
  TGRTCT1["trigger_changetarget"]
  style TGRTCT1 stroke:magenta,color:magenta;
  MM1["multi_manager 1"]
  style MM1 stroke:orange,color:orange;

  TCT2["trigger_changetarget"]
  style TCT2 stroke:magenta,color:magenta;
  TGRTCT2["trigger_changetarget"]
  style TGRTCT2 stroke:magenta,color:magenta;
  MM2["multi_manager 2"]
  style MM2 stroke:orange,color:orange;

  TCT3["trigger_changetarget"]
  style TCT3 stroke:magenta,color:magenta;
  TGRTCT3["trigger_changetarget"]
  style TGRTCT3 stroke:magenta,color:magenta;
  MM3["multi_manager 3"]
  style MM3 stroke:orange,color:orange;

  TT -.-> TGT1
  linkStyle 0 stroke:lime,stroke-dasharray:10;
  TT -.-> TGT2
  linkStyle 1 stroke:lime,stroke-dasharray:10;
  TT -.-> TGT3
  linkStyle 2 stroke:lime,stroke-dasharray:10;

  TCT1 --sets target 2--> TT
  linkStyle 3 stroke:magenta;
  TCT2 --sets target 3--> TT
  linkStyle 4 stroke:magenta;
  TCT3 --sets target 1--> TT
  linkStyle 5 stroke:magenta;

  MM1 --delay--> TCT1
  linkStyle 6 stroke:orange;
  MM1 --> TGRTCT1
  linkStyle 7 stroke:orange;

  MM2 --delay--> TCT2
  linkStyle 8 stroke:orange;
  MM2 --> TGRTCT2
  linkStyle 9 stroke:orange;

  MM3 --delay--> TCT3
  linkStyle 10 stroke:orange;
  MM3 --> TGRTCT3
  linkStyle 11 stroke:orange;
end

TGRTCT1 --sets multi_manager 2--> TGR
linkStyle 12 stroke:magenta;
TGRTCT2 --sets multi_manager 3--> TGR
linkStyle 13 stroke:magenta;
TGRTCT3 --sets multi_manager 1--> TGR
linkStyle 14 stroke:magenta;

TGR -.-> MM1
```
