#include "MultiBhopBlock.hpp"

#include <Utils.hpp>
#include <fmt/format.h>
#include <functional>

tl::expected<std::string, std::string> MultiBhopBlock::serialize() const
{
  try {
    const auto destination = getOrDefault("destination");

    if (destination.empty()) {
      throw std::runtime_error {
          fmt::format("{} requires a destination", ClassName)};
    }

    const auto delay = getOrDefault<double>("delay");

    if (delay == 0.0) {
      throw std::runtime_error {fmt::format("{} delay cannot be 0", ClassName)};
    }

    const auto originalBrushes = getBrushes();

    std::function<std::vector<Brush>()> tpBrushesFunc;

    tpBrushesFunc = [&originalBrushes] {
      auto brushes = replace_brushes_textures(originalBrushes);

      for (auto &brush : brushes) {
        for (auto &face : brush) {
          face.point1.z++;
          face.point2.z++;
          face.point3.z++;
        }
      }

      return brushes;
    };

    return fmt::format(
        R"(
{{
{layer}"classname" "func_button"
"spawnflags" "257"
"wait" "0"
"target" "{original_name}_mm"
{button_brushes}
}}

{{
{layer}"classname" "trigger_teleport"
"target" "{original_name}_itgt"
"targetname" "{original_name}_tt"
{tp_brushes}
}}

{{
{layer}"classname" "trigger_changetarget"
"origin" "{origin}"
"target" "{original_name}_tt"
"targetname" "{original_name}_ict"
"m_iszNewTarget" "{original_name}_itgt"
}}

{{
{layer}"classname" "trigger_changetarget"
"origin" "{origin}"
"target" "{original_name}_tt"
"targetname" "{original_name}_vct"
"m_iszNewTarget" "{destination}"
}}

{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}_mm"
"{original_name}_vct" "0.1"
"{original_name}_ict" "{delay:.{decimals_count}f}"
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("original_name", getName()),
        fmt::arg("button_brushes", brushes_to_str(originalBrushes)),
        fmt::arg("tp_brushes", brushes_to_str(tpBrushesFunc())),
        fmt::arg("origin", get_origin(originalBrushes)),
        fmt::arg("destination", destination),
        fmt::arg("delay", delay + 0.1),
        fmt::arg("decimals_count", DecimalsCount));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &MultiBhopBlock::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"destination",
                   "",
                   "Where to teleport player",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {
          "delay", 0.1, "Delay before trigger", OwnProperty::Type::String, {}},
  };

  return props;
}

REGISTER_PLUGIN(MultiBhopBlock)
