# MultiBhopBlock

A static bunnyhop block on which you need to constantly jump in order to stay on it.

## Properties

| Property      | Default value | Description                                                     |
| ------------- | ------------- | --------------------------------------------------------------- |
| `targetname`  |               | Name                                                            |
| `destination` |               | Where to teleport player if he stops bhopping                   |
| `delay`       | `0.1`         | How long the player is allowed to touch the block between bhops |

## Generates

```mermaid
graph TD;

DST["destination"]

subgraph remec_multi_bhop_block
BTN["func_button"]
style BTN stroke:red,color:red;
TT["trigger_teleport"]
style TT stroke:lime,color:lime;
TCT1["trigger_changetarget"]
style TCT1 stroke:magenta,color:magenta;
TCT2["trigger_changetarget"]
style TCT2 stroke:magenta,color:magenta;
MM["multi_manager"]
style MM stroke:orange,color:orange;

BTN --> MM
linkStyle 0 stroke:red;
MM --0.1s--> TCT1
linkStyle 1 stroke:orange;
MM --0.1s + delay--> TCT2
linkStyle 2 stroke:orange;
TCT1 -.changes TP target to destination.-> TT
linkStyle 3 stroke:magenta,stroke-dasharray:10;
TCT2 -.disables TP by changing target to an invalid one.-> TT
linkStyle 4 stroke:magenta,stroke-dasharray:10;
end

TT -.-> DST
linkStyle 5 stroke:lime,stroke-dasharray:10;
```
