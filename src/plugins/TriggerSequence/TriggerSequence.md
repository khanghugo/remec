# remec_trigger_sequence

Trigger a series of targets in specific sequences.
Useful when you want a target, for example a button, to trigger something different on each press.

## Properties

| Property     | Default value | Description                                     |
| ------------ | ------------- | ----------------------------------------------- |
| `targetname` |               | Name                                            |
| `target1`    |               | Target name; value is delay in seconds          |
| `target2`    |               | Target name; value is delay in seconds          |
| `target3`    |               | Target name; value is delay in seconds          |
| ...          |               | More targets                                    |
| `sequence`   |               | Space separated list of target names to trigger |

**NOTE**: target1, 2, 3 are just examples, replace them with your target entity names

## Generates

**Number of entites**: _3 \* number of targets + 1_

```mermaid
graph TD;

TGT1["target1"];
TGT2["target2"];
TGT3["target3"];

subgraph remec_trigger_sequence
  TR["trigger_relay"]
  style MM1 stroke:green,color:green;

  MM1["multi_manager [1]"]
  style MM1 stroke:orange,color:orange;
  TCT1["trigger_changetarget [1]"]
  style TCT1 stroke:magenta,color:magenta;

  TR -----> MM1
  MM1 -----> TCT1
  TCT1 -.changes target to target2.-> TR

  MM2["multi_manager [2]"]
  style MM2 stroke:orange,color:orange;
  TCT2["trigger_changetarget [2]"]
  style TCT2 stroke:magenta,color:magenta;

  TR -----> MM2
  MM2 -----> TCT2
  TCT2 -.changes target to target3.-> TR

  MM3["multi_manager [3]"]
  style MM3 stroke:orange,color:orange;
  TCT3["trigger_changetarget [3]"]
  style TCT3 stroke:magenta,color:magenta;

  TR -----> MM3
  MM3 -----> TCT3
  TCT3 -.changes target to target1.-> TR
end

MM1 ----> TGT1
MM2 ----> TGT2
MM3 ----> TGT3
```
