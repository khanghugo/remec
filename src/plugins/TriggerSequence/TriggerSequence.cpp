#include "TriggerSequence.hpp"

#include "Utils.hpp"

#include <fmt/format.h>

tl::expected<std::string, std::string> TriggerSequence::serialize() const
{
  try {
    const auto originalName = getName();
    auto targets            = getFilteredProperties();
    const auto origin       = targets.extract("origin");

    if (targets.size() < 2) {
      throw std::runtime_error {
          fmt::format("{} requires at least 2 targets", ClassName)};
    }

    const auto sequenceStr = getOrDefault("sequence");

    if (sequenceStr.empty()) {
      throw std::runtime_error {
          fmt::format("{} requires a sequence", ClassName)};
    }

    const auto sequence = split(sequenceStr, ' ');

    if (sequence.empty()) {
      throw std::runtime_error {
          fmt::format("{} requires a space separated sequence", ClassName)};
    }

    if (sequence.size() != std::set(sequence.begin(), sequence.end()).size()) {
      throw std::runtime_error {
          fmt::format("{} sequence cannot have duplicates", ClassName)};
    }

    std::string sequenceMMs;

    for (auto i = 0u; i < sequence.size(); ++i) {
      const auto &target = sequence[i];

      if (not targets.count(target)) {
        throw std::runtime_error {
            fmt::format("'{}' is not a valid target", target)};
      }

      std::string targetSequence;

      if (i + 1 < sequence.size()) {
        targetSequence = sequence[i + 1];
      }
      else {
        targetSequence = sequence[0];
      }

      const auto newTargetName =
          fmt::format("{}_mm_{}", originalName, targetSequence);

      sequenceMMs += fmt::format(R"(
{{
{layer}"classname" "trigger_changetarget"
"targetname" "{original_name}_tct_{actual_target}"
"origin" "{origin}"
"target" "{original_name}"
"m_iszNewTarget" "{new_target}"
}}

{{
{layer}"classname" "multi_manager"
"targetname" "{original_name}_mm_{actual_target}"
"origin" "{origin}"
"spawnflags" "0"
"{actual_target}" "{delay}"
"{original_name}_tct_{actual_target}" "0"
}}
)",
                                 fmt::arg("layer", getLayer()),
                                 fmt::arg("origin", origin.mapped()),
                                 fmt::arg("original_name", originalName),
                                 fmt::arg("actual_target", target),
                                 fmt::arg("new_target", newTargetName),
                                 fmt::arg("delay", targets.at(target)));
    }

    return fmt::format(
        R"(
{{
{layer}"classname" "trigger_relay"
"targetname" "{original_name}"
"origin" "{origin}"
"spawnflags" "0"
"triggerstate" "0"
"target" "{original_name}_mm_{first_target}"
}}

{sequences_mms}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("origin", origin.mapped()),
        fmt::arg("original_name", originalName),
        fmt::arg("sequences_mms", sequenceMMs),
        fmt::arg("first_target", sequence.front()));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &TriggerSequence::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"sequence",
                   "",
                   "Sequence in which to trigger targets",
                   OwnProperty::Type::String,
                   {}},
  };

  return props;
}

REGISTER_PLUGIN(TriggerSequence)
