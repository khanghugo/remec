#pragma once

#include <common/CustomEntity.hpp>

class AutoResetSwitch final : public CustomEntity<AutoResetSwitch> {
public:
  using CustomEntity<AutoResetSwitch>::CustomEntity;

  static constexpr inline auto ClassName = "remec_auto_reset_switch";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "Switches target on and off after a delay";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
