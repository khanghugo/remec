#pragma once

#include <common/CustomEntity.hpp>

class SlipRamp final : public CustomEntity<SlipRamp> {
public:
  using CustomEntity<SlipRamp>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_slip_ramp";
  static constexpr inline auto Version             = "2.0.0";
  static constexpr inline auto Description         = "Slippery surf ramp";
  static constexpr inline auto TargetNameMandatory = false;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
