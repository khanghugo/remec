#include "PluginEntityFactoryImpl.hpp"

#include "SharedLibraryUtils.hpp"
#include "Utils.hpp"
#include "common/LogWrapper.hpp"
#include "tracing/ITracer.hpp"

#include <common/Entity.hpp>
#include <fmt/format.h>

extern spdlog::logger *g_logger;

void PluginEntityFactoryImpl::LibraryCloser::operator()(void *lib) const
{
  close_library(lib);
};

PluginEntityFactoryImpl::PluginEntityFactoryImpl(
    std::filesystem::path halfLifePath,
    std::vector<std::filesystem::path> pluginLibrariesPaths,
    std::string_view tbOrJack)
    : m_halfLifePath {std::move(halfLifePath)}
    , m_tbOrJack {tbOrJack}
{
  if (not pluginLibrariesPaths.empty()) {
    logging(Level::Info, "Available plugins:");
  }

  for (const auto &libPath : pluginLibrariesPaths) {
    auto lib = open_library(libPath);

    m_pluginsLibs.push_back(
        std::unique_ptr<void, LibraryCloser>(lib, LibraryCloser {}));

    const auto classNameFunc =
        get_library_function_T<className_t>(lib, className_s);
    const auto versionFunc = get_library_function_T<version_t>(lib, version_s);

    logging(Level::Info,
            "'{}': {}",
            asColor<Color::blue>(classNameFunc()),
            asColor<Color::green>(versionFunc()));
  }
}

std::unordered_map<std::string, bool>
PluginEntityFactoryImpl::getSolidMapping() const
{
  std::unordered_map<std::string, bool> results;

  for (auto &pluginLib : m_pluginsLibs) {
    const auto classNameFunc =
        get_library_function_T<className_t>(pluginLib.get(), className_s);
    const auto solidFunc =
        get_library_function_T<solid_t>(pluginLib.get(), solid_s);

    results[classNameFunc()] = solidFunc();
  }

  return results;
}

void *PluginEntityFactoryImpl::getLibraryForClassName(
    std::string_view className) const
{
  for (auto &pluginLib : m_pluginsLibs) {
    const auto classNameFunc =
        get_library_function_T<className_t>(pluginLib.get(), className_s);

    if (classNameFunc() == className) {
      return pluginLib.get();
    }
  }

  throw std::runtime_error {
      fmt::format("REMEC cannot handle entities of type '{}' !", className)};
}

IPluginSmartPtr
PluginEntityFactoryImpl::makeEntity(std::string_view str,
                                    std::size_t originalStartPos,
                                    std::size_t originalLen,
                                    std::string_view skyName) const
{
  REMEC_TRACE(Category::remec_entity_factory, "makeEntity");

  static std::unordered_map<std::string, bool> solidMapping;

  if (solidMapping.empty()) {
    solidMapping = getSolidMapping();
  }

  const auto currentEntityStr = str.substr(originalStartPos, originalLen);

  auto [properties, brushes] = parse_entity(currentEntityStr, solidMapping);

  const auto entityClassName =
      properties.count("classname") ? properties.at("classname") : "";

  if (entityClassName.empty()) {
    throw std::runtime_error {fmt::format("Empty REMEC entity class !")};
  }

  REMEC_TRACE(Category::remec_entity_factory, entityClassName);

  auto entityLibrary = getLibraryForClassName(entityClassName);

  static const Context context {
      std::string(skyName), m_halfLifePath, m_tbOrJack, g_logger, g_tracer};

  const auto newInstanceFunc =
      get_library_function_T<newInstance_t>(entityLibrary, newInstance_s);
  const auto deleteInstanceFunc =
      get_library_function_T<deleteInstance_t>(entityLibrary, deleteInstance_s);

  auto newEntity = IPluginSmartPtr {newInstanceFunc(context,
                                                    originalStartPos,
                                                    originalLen,
                                                    std::move(properties),
                                                    brushes),
                                    PluginDeleter(deleteInstanceFunc)};

  if (newEntity->isTargetNameMandatory()) {
    const auto targetNameNotSet = newEntity->getName().empty();

    if (targetNameNotSet) {
      throw std::runtime_error {
          fmt::format("Missing 'targetname' property for entity class '{}' !",
                      entityClassName)};
    }
  }

  return newEntity;
}