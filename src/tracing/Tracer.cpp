#include "Tracer.hpp"

#include "categories.hpp"
#include "common/LogWrapper.hpp"
#include "perfetto/perfetto.h"

#include <cstdio>

namespace
{
void InitializePerfetto()
{
  perfetto::TracingInitArgs args;
  args.backends = perfetto::kInProcessBackend;

  perfetto::Tracing::Initialize(args);
  perfetto::TrackEvent::Register();
}

std::unique_ptr<perfetto::TracingSession> StartTracing(int fd)
{
  perfetto::TraceConfig cfg;
  cfg.add_buffers()->set_size_kb(1024 * 10);

  auto ds_cfg = cfg.add_data_sources()->mutable_config();
  ds_cfg->set_name("track_event");

  auto tracing_session = perfetto::Tracing::NewTrace();

  tracing_session->Setup(cfg, fd);
  tracing_session->StartBlocking();

  auto process_track = perfetto::ProcessTrack::Current();
  auto desc          = process_track.Serialize();

  desc.mutable_process()->set_process_name("remec");

  perfetto::TrackEvent::SetTrackDescriptor(process_track, desc);

  return tracing_session;
}

void StopTracing(
    const std::unique_ptr<perfetto::TracingSession> &tracing_session)
{
  perfetto::TrackEvent::Flush();

  tracing_session->StopBlocking();
}

std::string category_to_str(const Category category)
{
  switch (category) {
  case Category::remec:
    return "remec";

  case Category::remec_plugin:
    return "remec.plugin";

  case Category::remec_plugin_discovery:
    return "remec.plugin.discovery";

  case Category::remec_entity_factory:
    return "remec.entity.factory";

  case Category::remec_update_manager:
    return "remec.update.manager";

  case Category::remec_rewriter:
    return "remec.rewriter";

  case Category::remec_util:
    return "remec.util";
  }

  throw std::runtime_error {"unknown.category"};
}

} // namespace

Tracer::Tracer()
    : m_fd {std::fopen((BasePath + "/remec.trace").c_str(), "w")}
{
  logging(Level::Debug, "Initializing perfetto");

  InitializePerfetto();

  m_session = StartTracing(fileno(m_fd));
}

static std::uint64_t g_runningTraces = 0;

Tracer::~Tracer()
{
  StopTracing(m_session);

  std::fclose(m_fd);

  if (g_runningTraces != 0) {
    logging(Level::Error, "Not all traces ended '{}'", g_runningTraces);
  } else {
    logging(Level::Debug, "Stopped tracing");
  }
}

void Tracer::beginTrace(
    Category category,
    std::string_view name,
    const std::optional<std::uint64_t> &id,
    const std::unordered_map<std::string, std::string> &params) const
{
  ++g_runningTraces;

  perfetto::DynamicCategory cat {category_to_str(category)};

  if (id) {
    if (params.empty()) {
      TRACE_EVENT_BEGIN(
          cat, perfetto::DynamicString(name.data()), perfetto::Track {*id});
    } else {
      TRACE_EVENT_BEGIN(cat,
                        perfetto::DynamicString(name.data()),
                        perfetto::Track {*id},
                        [params](perfetto::EventContext ctx) {
                          for (const auto &[key, value] : params) {
                            ctx.AddDebugAnnotation(
                                perfetto::DynamicString(key),
                                perfetto::DynamicString(value));
                          }
                        });
    }
  } else {
    if (params.empty()) {
      TRACE_EVENT_BEGIN(cat, perfetto::DynamicString(name.data()));
    } else {
      TRACE_EVENT_BEGIN(cat,
                        perfetto::DynamicString(name.data()),
                        [params](perfetto::EventContext ctx) {
                          for (const auto &[key, value] : params) {
                            ctx.AddDebugAnnotation(
                                perfetto::DynamicString(key),
                                perfetto::DynamicString(value));
                          }
                        });
    }
  }
}

void Tracer::endTrace(Category category) const
{
  --g_runningTraces;

  perfetto::DynamicCategory cat {category_to_str(category)};

  TRACE_EVENT_END(cat);
}