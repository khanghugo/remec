#include "util/RGB.hpp"

bool RGB::operator==(const RGB &other) const noexcept
{
  return r == other.r and g == other.g and b == other.b;
}

bool RGB::operator<(const RGB &other) const noexcept
{
  if (r != other.r)
    return r < other.r;

  if (g != other.g)
    return g < other.g;

  return b < other.b;
}
