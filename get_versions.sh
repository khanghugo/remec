#!/bin/bash

set -e

pushd src/plugins &> /dev/null
PLUGINS_LIST=`ls -1`
popd &> /dev/null

echo "Add the following to the END of the release notes:"
echo ""

echo "#### Plugin versions"
for plugin in `echo ${PLUGINS_LIST}`;
do
    CLASSNAME=$(grep "ClassName" src/plugins/${plugin}/${plugin}.hpp | cut -d'"' -f2)
    VERSION=$(grep "Version" src/plugins/${plugin}/${plugin}.hpp | cut -d'=' -f2 | cut -d'"' -f2)

    echo "${CLASSNAME}:${VERSION}"
done
