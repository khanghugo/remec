import json
import os
from os import environ
from os.path import exists
import requests

headers = {"PRIVATE-TOKEN": environ["GITLAB_KEY"]}


def getNextTag():
    url = "https://www.gitlab.com/api/v4/projects/37711736/repository/tags"

    jsonObj = json.loads(requests.get(url, headers=headers).text)

    if len(jsonObj) == 0:
        lastTag = "v0"
    else:
        lastTag = jsonObj[0]["name"]

    nextTag = "v" + str(int(lastTag[1:]) + 1)

    return nextTag


def createNewTag(name):
    url = f"https://www.gitlab.com/api/v4/projects/37711736/repository/tags?tag_name={name}&ref=master"

    print(f"[  4] Creating tag {name}...")

    response = requests.post(url, headers=headers)

    if response.status_code == 405:
        print("Create tags failed", response.reason)
        exit(1)


def deleteTag(name):
    url = f"https://www.gitlab.com/api/v4/projects/37711736/repository/tags/{name}"

    print(f"[ -1] Deleting tag {name}...")

    requests.delete(url, headers=headers)


def uploadFile(file):
    url = "https://www.gitlab.com/api/v4/projects/37711736/uploads"
    files = {"file": open(file, "rb")}

    print(f"[  5] Uploading {file}...")

    response = requests.post(url, files=files, headers=headers)

    try:
        jsonObj = json.loads(response.text)

        return jsonObj["full_path"]
    except BaseException:
        print(response.text)


def createRelease(tag, zipURL):
    url = "https://www.gitlab.com/api/v4/projects/37711736/releases"
    body = {
        "name": tag,
        "tag_name": tag,
        "assets": {
            "links": [
                {
                    "name": "remec.zip",
                    "url": f"https://gitlab.com/{zipURL}",
                    "link_type": "package",
                }
            ]
        },
    }

    print(f"[  6] Creating release {tag}...")

    return json.loads(requests.post(url, headers=headers, json=body).text)["_links"][
        "self"
    ]


release_build = "./release_build"
zippath = f"./{release_build}/remec.zip"

if exists(zippath):
    nextTag = getNextTag()

    createNewTag(nextTag)

    remecZipURL = uploadFile(zippath)

    if remecZipURL and len(remecZipURL) != 0:
        releaseURL = createRelease(nextTag, remecZipURL)

        if len(releaseURL) != 0:
            print(releaseURL)

            try:
                os.remove(zippath)
                os.rmdir(release_build)
            except OSError as e:
                print("Error", e.strerror)
        else:
            deleteTag(nextTag)
    else:
        deleteTag(nextTag)
